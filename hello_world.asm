;this is a comment

section .data
	text db "Hello World!", 10 ; text = giving a name to memory address which stores the data, db = define bytes basiclly like defining variable as str, 10 = the code of "\n"


section .text ; section with the actual code

	global _start ;this is marker for the linker (ld) where to start

_start
	mov rax, 1
	mov rdi, 1
	mov rsi, text
	mov rdx, 14
	syscall
	
	mov rax, 60
	mov rdi, 0
	syscall

	
;the code in assembly organized to sections like this:

;section .data ;section which stores data and variables you going to use in your code.

;section .text ;the main section, section which stores the code



